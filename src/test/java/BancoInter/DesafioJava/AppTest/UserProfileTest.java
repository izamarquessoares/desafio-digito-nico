package BancoInter.DesafioJava.AppTest;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import BancoInter.DesafioJava.Criptografia.CriptografiaService;
import BancoInter.DesafioJava.DigitoUnico.DigitoUnicoService;
import BancoInter.DesafioJava.UserProfile.UserProfile;
import BancoInter.DesafioJava.UserProfile.UserProfileRepository;
import BancoInter.DesafioJava.UserProfile.UserProfileService;

@ExtendWith(MockitoExtension.class)
class UserProfileTest {
	
	@InjectMocks
	private UserProfileService userProfileService;
	
	@Mock
	private UserProfileRepository userProfileRepository;
	
	@Mock
	private DigitoUnicoService digitoUnicoService;
	
	@Mock
	private CriptografiaService criptografiaService;
	
	private UserProfile usuario = new UserProfile();

	@Test
	void TestAdd() {
		userProfileService.save(usuario);
		verify(userProfileRepository, times(1)).save(usuario);
	}

	@Test
	void TestGetAll() {
		userProfileService.getAll();
		verify(userProfileRepository, times(1)).findAll();

	}

	@Test
	void TestFindById() {
		userProfileService.findById(1);
		verify(userProfileRepository, times(1)).findUserProfileById(1);

	}

	@Test
	void TestUpdate() throws Exception {
		userProfileService.update(usuario);
		verify(userProfileRepository, times(1)).save(usuario);

	}

	@Test
	void TestRemove() throws Exception {
		userProfileService.remove(1);
		verify(userProfileRepository, times(1)).deleteById(1);

	}
}
