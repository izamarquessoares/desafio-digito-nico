package BancoInter.DesafioJava.AppTest;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import BancoInter.DesafioJava.Criptografia.Criptografia;
import BancoInter.DesafioJava.Criptografia.CriptografiaRepository;
import BancoInter.DesafioJava.Criptografia.CriptografiaService;

@ExtendWith(MockitoExtension.class)
public class CriptografiaTest {

	@InjectMocks
	private CriptografiaService criptografiaService;

	@Mock
	private CriptografiaRepository criptografiaRepository;

	Criptografia criptografia = new Criptografia();

	@Test
	void TestAdd() {
		criptografiaService.add(criptografia);
		verify(criptografiaRepository, times(1)).save(criptografia);
	}

	@Test
	void TestFindByUserId() {
		criptografiaService.findByUserId(1);
		verify(criptografiaRepository, times(1)).findByUserProfileId(1);
	}

}
