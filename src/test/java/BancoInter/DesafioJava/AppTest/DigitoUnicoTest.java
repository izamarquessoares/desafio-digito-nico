package BancoInter.DesafioJava.AppTest;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import BancoInter.DesafioJava.DigitoUnico.DigitoUnico;
import BancoInter.DesafioJava.DigitoUnico.DigitoUnicoRepository;
import BancoInter.DesafioJava.DigitoUnico.DigitoUnicoService;

@ExtendWith(MockitoExtension.class)
 class DigitoUnicoTest {
	
	@InjectMocks
	private  DigitoUnicoService digitoUnicoService;
	
	@Mock
	public DigitoUnicoRepository digitoUnicoRepository;

	private final String numeroParaConcatenar = "1313";
	private String resultadoEsperado = "", resultadoObtido = "";
	private int numeroDeRepeticao = 2, resultado = 7;
	private DigitoUnico digitoUnicoTest = new DigitoUnico(numeroParaConcatenar, numeroDeRepeticao);

	@Test
	void TestGerarDigito() {

		Integer resultadoObtido = digitoUnicoService.GerarDigitoUnico(digitoUnicoTest);
		Integer resultadoEsperado = 7;
		assertEquals(resultadoEsperado, resultadoObtido);
	}

	@Test
	void TestConcatenarNumeroRepeticaoZero() {
		/**
		 * Teste com o numero 0
		 */
		numeroDeRepeticao = 0;
		resultadoEsperado = "0";
		resultadoObtido = DigitoUnicoService.ConcatenarNumero(numeroParaConcatenar, numeroDeRepeticao);
		assertEquals(resultadoEsperado, resultadoObtido);

	}

	@Test
	void TestConcatenarNumeroRepeticaoUm() {
		/**
		 * Teste com a repetição uma vez
		 */
		numeroDeRepeticao = 1;
		resultadoEsperado = numeroParaConcatenar;
		resultadoObtido = DigitoUnicoService.ConcatenarNumero(numeroParaConcatenar, numeroDeRepeticao);
		assertEquals(resultadoEsperado, resultadoObtido);

	}

	@Test
	void TestConcatenarNumeroRepeticaoMaiorUm() {
		/**
		 * Teste com a repetição uma vez
		 */
		numeroDeRepeticao = 1;
		resultadoEsperado = numeroParaConcatenar;
		resultadoObtido = DigitoUnicoService.ConcatenarNumero(numeroParaConcatenar, numeroDeRepeticao);
		assertEquals(resultadoEsperado, resultadoObtido);

	}

	@Test
	void TestCalcularDigito() {

		Integer resultadoEsperado = 8;
		Integer resultadoObtido = DigitoUnicoService.CalcularDigito(Integer.parseInt(numeroParaConcatenar));
		assertEquals(resultadoEsperado, resultadoObtido);

	}

	@Test
	void TestAddCache() {

		digitoUnicoTest.setResultado(resultado);
		DigitoUnicoService.addCache(digitoUnicoTest);
		assertSame(DigitoUnicoService.cache.get(0), digitoUnicoTest);

	}

	@Test
	void TestVerificaCacheTrue() {

		digitoUnicoTest.setResultado(resultado);
		DigitoUnicoService.addCache(digitoUnicoTest);
		boolean achou = DigitoUnicoService.VerificaCache(digitoUnicoTest);
		assertTrue(achou);

	}

	@Test
	void TestVerificaCacheFalse() {

		DigitoUnico digitoUnicoTestFalse = new DigitoUnico("1414", 2);
		boolean Naoachou = DigitoUnicoService.VerificaCache(digitoUnicoTestFalse);
		assertFalse(Naoachou);

	}

	@Test
	void TestFindById() {
		digitoUnicoService.findByUserId(1);
		verify(digitoUnicoRepository, times(1)).findByUserProfileId(1);

	}

}
