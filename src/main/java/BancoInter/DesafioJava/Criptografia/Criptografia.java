package BancoInter.DesafioJava.Criptografia;

import java.security.PrivateKey;
import java.security.PublicKey;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import BancoInter.DesafioJava.UserProfile.UserProfile;

@Entity
@Table(name = "criptografia")
public class Criptografia {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "chave_privada", length = 2000)
	private PrivateKey chavePrivada;

	@Column(name = "chave_publica", length = 1000)
	private PublicKey chavePublica;

	@OneToOne()
	@JoinColumn(name = "user_profile_id", referencedColumnName = "id")
	private UserProfile userProfile;

	public PrivateKey getChavePrivada() {
		return chavePrivada;
	}

	public void setChavePrivada(PrivateKey chavePrivada) {
		this.chavePrivada = chavePrivada;
	}

	public PublicKey getChavePublica() {
		return chavePublica;
	}

	public void setChavePublica(PublicKey chavePublica) {
		this.chavePublica = chavePublica;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}
}
