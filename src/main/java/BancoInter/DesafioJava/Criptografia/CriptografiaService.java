package BancoInter.DesafioJava.Criptografia;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import BancoInter.DesafioJava.UserProfile.UserProfile;

@Service
public class CriptografiaService {

	@Autowired
	private CriptografiaRepository criptografiaRepository;

	public void gerarChave(UserProfile user) {
		try {
			final KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(2048);
			final KeyPair key = keyGen.generateKeyPair();
			Criptografia criptografia = new Criptografia();
			criptografia.setChavePrivada(key.getPrivate());
			criptografia.setChavePublica(key.getPublic());
			criptografia.setUserProfile(user);
			add(criptografia);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param texto
	 * @return
	 */
	public byte[] criptografa(String texto, Integer userId) {
		byte[] cipherText = null;

		try {
			final PublicKey chavePublica = findByUserId(userId).getChavePublica();

			final Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, chavePublica);
			cipherText = cipher.doFinal(texto.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cipherText;
	}

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public Criptografia findByUserId(Integer userId) {
		return criptografiaRepository.findByUserProfileId(userId);
	}
	
	/**
	 * 
	 * @param userId
	 */
	public void deleteByUserId(Integer userId) {
		criptografiaRepository.delete(findByUserId(userId));
	}

	/**
	 * 
	 * @param texto
	 * @return
	 */
	public String decriptografa(byte[] texto, Integer userId) {
		byte[] dectyptedText = null;
		try {
			final PrivateKey chavePrivada = findByUserId(userId).getChavePrivada();

			final Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, chavePrivada);
			dectyptedText = cipher.doFinal(texto);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return new String(dectyptedText);
	}

	public Criptografia add(Criptografia criptografia) {
		return criptografiaRepository.save(criptografia);
	}

}