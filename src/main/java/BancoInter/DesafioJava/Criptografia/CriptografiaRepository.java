package BancoInter.DesafioJava.Criptografia;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CriptografiaRepository extends JpaRepository<Criptografia, Integer> {

	Criptografia findByUserProfileId(Integer userId);

}