package BancoInter.DesafioJava.DigitoUnico;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("api/digito-unico")
public class DigitoUnicoController {

	@Autowired
	private DigitoUnicoService digitoUnicoService;

	@ApiOperation(value="Gerar o dígito único de um número")
	@PutMapping("gerardigito")
	public int GerarDigito(@RequestBody DigitoUnico digitoUnico) {

		return digitoUnicoService.GerarDigitoUnico(digitoUnico);
	}

	@ApiOperation(value="Busca os digitos único de um determinado usuário")
	@GetMapping("getByUserId/{id}")
	public ResponseEntity<List<DigitoUnico>> findById(@PathVariable Integer id) {
		List<DigitoUnico> digitosUnicoUser = digitoUnicoService.findByUserId(id);
		return ResponseEntity.ok(digitosUnicoUser);
	}

}
