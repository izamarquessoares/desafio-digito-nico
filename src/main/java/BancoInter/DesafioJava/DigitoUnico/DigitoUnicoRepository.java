package BancoInter.DesafioJava.DigitoUnico;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Integer> {

	@Query("SELECT new BancoInter.DesafioJava.DigitoUnico.DigitoUnico(digitoUnico.codigoDigitoUnico, digitoUnico.numero, digitoUnico.repeticao, digitoUnico.resultado) from DigitoUnico digitoUnico where digitoUnico.userProfile.id = :idUserProfile")
	List<DigitoUnico> findByUserProfileId(@Param("idUserProfile") Integer idUserProfile);
}
