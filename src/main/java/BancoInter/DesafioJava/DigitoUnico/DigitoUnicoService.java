package BancoInter.DesafioJava.DigitoUnico;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DigitoUnicoService {

	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;

	public static List<DigitoUnico> cache;

	DigitoUnicoService() {
		cache = new ArrayList<>();
	}

	/**
	 * Concatena o número de acordo com um número de repetição
	 * 
	 * @param numeroParaConcatenar
	 * @param repete
	 * @return
	 */
	public static String ConcatenarNumero(String numeroParaConcatenar, int repete) {
		if (repete == 0)
			return "0";
		else if (repete == 1)
			return numeroParaConcatenar;

		String numeroConcatenado = "";
		for (int i = 0; i < repete; i++) {
			numeroConcatenado = numeroConcatenado + numeroParaConcatenar;
		}
		return numeroConcatenado;
	}

	/**
	 * Recebe o número e soma cada algorismo desse número
	 * 
	 * @param numeroInteiro
	 * @return
	 */
	public static int CalcularDigito(int numeroInteiro) {
		int resto, somatorio = 0;
		while (numeroInteiro > 0) {
			resto = numeroInteiro % 10;
			numeroInteiro = (numeroInteiro - resto) / 10;
			somatorio += resto;
		}
		if (somatorio >= 9)
			return CalcularDigito(somatorio);
		return somatorio;
	}

	/**
	 * Gera o número do dígito único
	 * 
	 * @param digitoUnico
	 * @return
	 */
	public int GerarDigitoUnico(DigitoUnico digitoUnico) {
		int resultado = 0;

		/// Verifica se está no cache, se não houver, faz os calculos;
		if (!VerificaCache(digitoUnico)) {
			resultado = Integer.parseInt(ConcatenarNumero(digitoUnico.getNumero(), digitoUnico.getRepeticao()));
		}
		if (resultado <= 9) {
			digitoUnico.setResultado(resultado);
			addCache(digitoUnico);
		} else {
			digitoUnico.setResultado(CalcularDigito(resultado));
			addCache(digitoUnico);
		}

		// So adiciono no banco se tiver um userProfile associado
		if (digitoUnico.getUserProfile() != null)
			addBanco(digitoUnico);
		return digitoUnico.getResultado();

	}

	/**
	 * Verifica se o número está cache
	 * 
	 * @param digitoUnico
	 * @return
	 */
	public static boolean VerificaCache(DigitoUnico digitoUnico) {
		for (DigitoUnico verificacao : cache) {
			if (verificacao.getNumero().equals(digitoUnico.getNumero())
					&& verificacao.getRepeticao() == digitoUnico.getRepeticao()) {
				digitoUnico.setResultado(verificacao.getResultado());
				return true;
			}

		}
		return false;
	}

	/**
	 * Adiciona o numero na lista do cache
	 * 
	 * @param digitoUnico
	 */

	public static void addCache(DigitoUnico digitoUnico) {
		if (cache.size() >= 10)
			cache.remove(0);

		cache.add(digitoUnico);

	}

	public void addBanco(DigitoUnico digitoUnico) {
		digitoUnicoRepository.save(digitoUnico);

	}

	/**
	 * Salva o número no banco
	 * 
	 * @param digito
	 * @return
	 */

	public List<DigitoUnico> findByUserId(Integer id) {
		return digitoUnicoRepository.findByUserProfileId(id);
	}

	/**
	 * 
	 * @param id
	 */
	public void deleteByUserId(Integer id) {
		digitoUnicoRepository.deleteAll(findByUserId(id));
	}

}
