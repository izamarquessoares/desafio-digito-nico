package BancoInter.DesafioJava.DigitoUnico;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import BancoInter.DesafioJava.UserProfile.UserProfile;

@Entity
@Table(name = "digitoUnico")
public class DigitoUnico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer codigoDigitoUnico;

	@Column(name = "numero")
	private String numero;

	@Column(name = "repeticao")
	private int repeticao;

	@Column(name = "resultado")
	private int resultado;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userProfile_id", foreignKey = @ForeignKey(name = "userProfile_fk"))
	private UserProfile userProfile;

	public DigitoUnico() {
	}
	
	public DigitoUnico(String numero, int repeticao) {
		this.numero = numero;
		this.repeticao = repeticao;
	}

	public DigitoUnico(Integer codigoDigitoUnico, String numero, int repeticao, int resultado) {
		this.codigoDigitoUnico = codigoDigitoUnico;
		this.numero = numero;
		this.repeticao = repeticao;
		this.resultado = resultado;
	}

	public Integer getCodigoDigitoUnico() {
		return codigoDigitoUnico;
	}

	public void setCodigoDigitoUnico(Integer codigoDigitoUnico) {
		this.codigoDigitoUnico = codigoDigitoUnico;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public int getRepeticao() {
		return repeticao;
	}

	public void setRepeticao(int repeticao) {
		this.repeticao = repeticao;
	}

	public int getResultado() {
		return resultado;
	}

	public void setResultado(int resultado) {
		this.resultado = resultado;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

}
