package BancoInter.DesafioJava.UserProfile;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("api/userProfile")
public class UserController {

	@Autowired
	private UserProfileService userProfileService;

	@ApiOperation(value = "Salva um usuário no banco")
	@PostMapping("create")
	public ResponseEntity<UserProfileModel> add(@RequestBody UserProfileModel user) {
		UserProfileModel userInsert = userProfileService.add(user);
		return ResponseEntity.ok(userInsert);
	}

	@ApiOperation(value = "Busca todos o usuários cadastrados")
	@GetMapping("getAll")
	public ResponseEntity<List<UserProfileModel>> findAll() {
		List<UserProfileModel> listUser = userProfileService.getAll();
		return ResponseEntity.ok(listUser);
	}

	@ApiOperation(value = "Busca um usuário por um id único")
	@GetMapping("getById/{id}")
	public ResponseEntity<UserProfileModel> findById(@PathVariable Integer id) {
		UserProfileModel userFind = userProfileService.getById(id);
		return ResponseEntity.ok(userFind);
	}

	@ApiOperation(value = "Remove um usuário pelo seu id")
	@DeleteMapping("remove/{id}")
	public ResponseEntity<Void> remove(@PathVariable Integer id) throws Exception {
		userProfileService.remove(id);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Alterar um usuário do banco pelo seu id")
	@PutMapping("update")
	public ResponseEntity<UserProfileModel> update(@RequestBody UserProfileModel user) throws Exception {
		UserProfileModel userInsert = userProfileService.update(user);
		return ResponseEntity.ok(userInsert);
	}

}
