package BancoInter.DesafioJava.UserProfile;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRepository extends JpaRepository<UserProfile, Integer> {
	/**
	 * Foi criado uma query normativa para pesquisar o user pelo id
	 * @param id
	 * @return
	 */
	UserProfile findUserProfileById(Integer id);

}
