package BancoInter.DesafioJava.UserProfile;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import BancoInter.DesafioJava.Criptografia.CriptografiaService;
import BancoInter.DesafioJava.DigitoUnico.DigitoUnicoService;

@Service
public class UserProfileService {
	@Autowired
	private UserProfileRepository userProfileRepository;
	@Autowired
	private CriptografiaService criptografiaService;
	@Autowired
	private DigitoUnicoService digitoUnicoService;

	/**
	 * Adiciona o usuário no banco
	 * 
	 * @param user
	 */
	public UserProfileModel add(UserProfileModel user) {
		UserProfile saved = save(getEntidade(user));
		criptografiaService.gerarChave(saved);
		user.setId(saved.getId());
		saved = update(getEntidadeCriptografada(user));
		return getModel(saved);
	}

	/**
	 * 
	 * @param user
	 * @return
	 */
	public UserProfile save(UserProfile user) {
		return userProfileRepository.save(user);
	}

	/**
	 * Busca todos os usuários cadastrados
	 * 
	 * @return
	 */
	public List<UserProfileModel> getAll() {
		List<UserProfileModel> users = new ArrayList<>();
		for (UserProfile entity : userProfileRepository.findAll()) {
			users.add(getModel(entity));
		}
		return users;
	}

	/**
	 * Busca o usuário por id único
	 * 
	 * @param id
	 * @return
	 */
	public UserProfileModel getById(Integer id) {
		UserProfile obj = userProfileRepository.findUserProfileById(id);
		return getModel(obj);
	}

	public UserProfile findById(Integer id) {
		return userProfileRepository.findUserProfileById(id);
	}

	/**
	 * Remove o usuario pelo id
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void remove(Integer id) throws Exception {
		try {
			digitoUnicoService.deleteByUserId(id);
			criptografiaService.deleteByUserId(id);
			userProfileRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new Exception("Id não encontrado!");
		}
	}

	public UserProfileModel update(UserProfileModel userUpdate) {
		UserProfile updated = update(getEntidadeCriptografada(userUpdate));
		return getModel(updated);
	}



	private UserProfile getEntidadeCriptografada(UserProfileModel user) {
		UserProfile entidade = new UserProfile();
		entidade.setId(user.getId());
		entidade.setEmail(criptografiaService.criptografa(user.getEmail(), user.getId()));
		entidade.setNome(criptografiaService.criptografa(user.getNome(), user.getId()));
		return entidade;

	}

	private UserProfile getEntidade(UserProfileModel user) {
		UserProfile entidade = new UserProfile();
		entidade.setId(user.getId());
		entidade.setEmail(user.getEmail().getBytes());
		entidade.setNome(user.getNome().getBytes());
		return entidade;

	}

	private UserProfileModel getModel(UserProfile entidade) {
		UserProfileModel user = new UserProfileModel();
		user.setId(entidade.getId());
		user.setEmail(criptografiaService.decriptografa(entidade.getEmail(), entidade.getId()));
		user.setNome(criptografiaService.decriptografa(entidade.getNome(), entidade.getId()));
		return user;

	}
	//------------------- Métodos para ter acesso aos teste ------------------------------------
	/**
	 * Update para o teste
	 * 
	 * @param userUpdate
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public UserProfile update(UserProfile userUpdate) {
		return userProfileRepository.save(userUpdate);
	}
	
	
}
