package  BancoInter.DesafioJava.UserProfile;
import  BancoInter.DesafioJava.DigitoUnico.DigitoUnico;
import java.util.List;

public class UserProfileModel {
	
	private int id;
	
	private String nome;
	
	private String email;
	
	private List<DigitoUnico> digitoUnico;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<DigitoUnico> getDigitoUnico() {
		return digitoUnico;
	}

	public void setDigitoUnico(List<DigitoUnico> digitoUnico) {
		this.digitoUnico = digitoUnico;
	}
}
