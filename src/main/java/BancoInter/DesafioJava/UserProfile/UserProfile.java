package BancoInter.DesafioJava.UserProfile;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import BancoInter.DesafioJava.DigitoUnico.DigitoUnico;

@Entity
@Table(name = "userProfile")
public class UserProfile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "nome", length = 256)
	private byte[] nome;

	@Column(name = "email", length = 256)
	private byte[] email;
	
	@OneToMany(mappedBy = "userProfile", fetch = FetchType.LAZY)
	private List<DigitoUnico> digitoUnico;
	
	public UserProfile() {
	}

	public UserProfile(byte[] nome, byte[] email) {
		this.nome = nome;
		this.email = email;

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte[] getNome() {
		return nome;
	}

	public void setNome(byte[] nome) {
		this.nome = nome;
	}

	public void setEmail(byte[] email) {
		this.email = email;
	}

	public byte[] getEmail() {
		return email;
	}

	public List<DigitoUnico> getDigitoUnico() {
		return digitoUnico;
	}

	public void setDigitoUnico(List<DigitoUnico> digitoUnico) {
		this.digitoUnico = digitoUnico;
	}
}
