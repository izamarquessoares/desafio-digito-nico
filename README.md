# Desafio Dígito Único
#### Regra da aplicação Regra da aplicação
O desafio do Dígito Único teve como objetivo receber um número e o índice que indica quantas repetições deve ser concatenado esse número. Após as implementações foram geradas algumas regras, sendo elas não descrita com na documentação:
-  Se o usuário digitar o número de repetição 0, a aplicação retornará 0 , pois pelo que foi descrito, seria o número * repetição;
- Se o usuário digitar o número de repetição 1, a aplicação passará para a função de calcular o digito ,o número de entrada sem ter que fazer um concatenação.
- Só será salvo no banco de dados, os dígitos que tiverem um usuário associado. Os dígitos sem usuário, será salvo somente cache.
- Os dados de email e nome serão salvos criptografados no banco.
- Teve a necessidade de criar uma tabela chamada criptografia com intuito de guardar as chaves privadas e públicas do banco.

#### Instalações 
- Baixar a IDE eclipse pelo link abaixo:
`<link>` : https://www.eclipse.org/downloads/
Na hora de instalar, selecione o **"Eclipse IDE for EnterPrise Java Developers"**
- Baixe o JDK ou JRE 
`<link>` :https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html

- Baixar o spring boot pelo eclipse
1. Clique no menu Help
2.  Pesquisa por Spring Tools 4
3. Clique em install

#### Como rodar o projeto

1. Clique no menu file .
2. Depois clique "Open Projects from File System".
4. Caso não apareça o "Project Explorer", clique em windows >> show view>> Project Explorer ou Package Explorer.
4. Para tornar a visualização do Project Explorer melhor, clique nos 3 pontinhos, selecione o menu Package Presentation e escolha Hierachircal.
3. Depois selecione a pasta do projeto (Desafio Java), clique com o botão direito, vá ate o menu "Run As" e selecione Spring Boot App..

OBS: Se a aplicação não rodar por causa da porta da aplicação, vá na pasta resouces, no arquivo application.properties e altere a variável server.port para uma porta válida, por exemplo 8080

#### Como acessar o banco 
O banco foi feito  em memória, dessa forma toda vez que rodar a aplicação, ele sobe. Quando a aplicação para de rodar, o banco some juntamente com os dados.
- Informações de acesso:
    - Driver Class: org.h2.Driver
	- JDBC URL: jdbc:h2:mem:DesafioInter
	- User Name: sa

Para acessar:
`<link>` : http://localhost:9090/h2
OBS: se a porta for alterada, no link da aplicação também terá que ser alterado

#### Como acessar o documentação da API]
A documentação foi feita no swagger.
Para acessar:
`<link>` : http://localhost:9090/swagger-ui.html#!/

 #### Como rodar os testes
Para implementar os testes, foi usado o Junit.
Para rodar o teste, clique com botão esquerdo na  pasta "DesafioJava", vá na parte de Run AS e selecione Junit Test.
Irá aparecer uma janela do Junit, com os status dos teste.

#### Como rodar o teste com o postman
O arquivo "postman_collection.json”está na raiz do projeto. Após baixar, abre o postman, clique no botão import, selecione o arquivo e importar.
Para rodar. clique no botão "runner".
Nessa janela de runner, selecione todos os métodos, menos **remove**, pois usaremos o id para gerar o dígito único associado ao userProfile.
Depois selecione, o remove do UserProfile.

#### Considerações Finais 
Após esse grande desafio, percebi que aprendi muitas coisas técnica, partindo da linguagem java até a construção de APIs. 
Gostaria que levasse em conta que sabia o básico de java, na verdade,  só sabia  a parte de POO. 
Enfim, agradeço pela oportunidade , conheci muitas coisas importante para área de TI. Qualquer dúvida, pode me contatar. Obrigada





